#include "types.h"
#include "mmu.h"
#include "x86.h"
#include "user.h"
#include "kthreads.h"

kthread_t
thread_create(void (*start)(void*), void * arg) {
  kthread_t thread;
  void* stack = (void*) malloc(PGSIZE);
  thread.stack = stack;
  thread.pid = clone(start, arg, stack);
  return thread;
}

int
thread_join(kthread_t thread) {
  int result = join(thread.pid);
  if (result == 0) {
    free(thread.stack);
    return 0;
  } else {
    return -1;
  }
}

void
lock_acquire(lock_t* lock) {
  while(xchg(&lock->locked, 1) != 0)
    ;
  __sync_synchronize();
}

void
lock_release(lock_t* lock) {
  __sync_synchronize();
  asm volatile("movl $0, %0" : "+m" (lock->locked) : );
}

void
init_lock(lock_t* lock) {
  lock->locked = 0;
}
