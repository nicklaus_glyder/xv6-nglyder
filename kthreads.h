typedef struct lock {
  uint locked;
} lock_t;

typedef struct kthread {
  uint pid;
  void* stack;
} kthread_t;

kthread_t         thread_create(void (*)(void*), void *);
int               thread_join(kthread_t);
void              lock_acquire(lock_t*);
void              lock_release(lock_t*);
void              init_lock(lock_t*);
