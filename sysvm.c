#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"

int
sys_shmem_access(void) {
  int page;
  if (argint(0, &page) < 0)
    return -1;
  struct proc *curproc = myproc();
  return share_mem_access(curproc->pgdir, page);
}

int
sys_shmem_count(void) {
  int page;
  if (argint(0, &page) < 0)
    return -1;
  return share_mem_count(page);  
}
