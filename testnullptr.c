#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

// This includes null ptr check in syscall space
// because kernel could write to guard page
void null_ptr_test() {
  // Create a file to stat
  int fd = open("small", O_CREATE|O_RDWR);
  write(fd, "aaaaaaaaaa", 10);
  close(fd);

  // This should return -1 since st is null, kernel refuses
  struct stat* st = 0;
  int ret = stat("small", st);
  if (ret == -1) {
    printf(1, "null ptr identified, should die with trap 14\n");
  }

  // Deref null-ptr in user space
  printf(1, "file size is = %d\n", st->size); // This will die trap 14
}

int main(int argc, char const *argv[]) {
  null_ptr_test();
  exit();
}
