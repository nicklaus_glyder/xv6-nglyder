#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"
#include "kthreads.h"

void thread_func(void* arg) {
  int* a = (int*) arg;
  printf(1, "What did I get? %d\n", *a);
  exit();
}

void sys_call_test() {
  int num = 15;
  kthread_t t1 = thread_create(thread_func, &num);
  int val = thread_join(t1);
  if (val == 0)
    printf(1, "Join returned successfully\n");
}

int main(int argc, char const *argv[]) {
  sys_call_test();
  exit();
}
