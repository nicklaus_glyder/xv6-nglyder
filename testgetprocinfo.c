#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

int stdout = 1;

void procinfotest(void)
{
  printf(stdout, "start procinfo test\n");

  int pid = fork();

  if (pid > 0) {
    printf(stdout, "Parent Process allocating memory\n");
    struct procinfo* procs = (struct procinfo*) malloc(sizeof(struct procinfo)*NPROC);

    int num = getprocinfo(procs);
    for(int i = 0; i < num; i++) {
      printf(stdout, "pid: %d, pname: %s\n", procs[i].pid, procs[i].pname);
    }

    if (num == 4) {
      printf(stdout, "found two OS procs and two user procs: PASSED\n");
    }

    wait();
    free(procs);
  } else {
    sleep(2);
    printf(stdout, "Child Process done sleeping\n");
    exit();
  }

}

int
main(int argc, char *argv[])
{
  printf(stdout, "testgetprocinfo starting\n");

  procinfotest();

  exit();
}
